# stm32f7_ci Continuous Integration (CI) example for STM32F746 board 

Travis CI - [![Build Status](https://travis-ci.com/gouravkalye/stm32f7_ci.svg?branch=master)](https://travis-ci.com/gouravkalye/stm32f7_ci)

CircleCI - [![CircleCI](https://circleci.com/gh/gouravkalye/stm32f7_ci.svg?style=svg)](https://circleci.com/gh/gouravkalye/stm32f7_ci)
